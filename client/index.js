class Chat {
    constructor() {
        var url = window.location.hostname;
        this.ws = new WebSocket("ws://" + url + ":3001");

        this.ws.onerror = (error) => {
            $("#connection_label").html("Not connected: error");
        };

        this.ws.onopen = () => {
            $("#connection_label").html("Handshake finished");
        };

        this.ws.onclose = function(message) {
            $("#connection_label").html("Not connected: closed connection");
        };
    }
}

var chat;
$(document).ready(function(){
    chat = new Chat();
});
